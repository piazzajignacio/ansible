#!/usr/bin/env python3.6


from os import listdir, readlink
from os.path import islink, join, basename
import subprocess
import re #modulo de regular expresions

def get_raw_already_created():
    raw_already_created = set()
    mypath = '/dev/oracleasm/'
    for f in listdir(mypath):
        fullfile = join(mypath, f)
        if islink(fullfile):
            target = basename(readlink(fullfile))[:-1]
            raw_already_created.add(target)
    return list(raw_already_created) ##convierte el set en una lista             


def get_lvm_disks():
    lvmdevice_already_created = []
    pvs = subprocess.Popen(["pvs"],
                           stdout=subprocess.PIPE,
                          )
    lines = [line for line in pvs.stdout]
    for line in lines[1:]:
        device = basename(line.split()[0])[:-1]
        lvmdevice_already_created.append(device)
    return lvmdevice_already_created

def get_all_present_devices():
    full_list_of_devices = [] ## this is the whole device's list
    lsscsi =  subprocess.Popen(["lsscsi", "-i"],
                               stdout=subprocess.PIPE,
                              )
    lines = [line for line in lsscsi.stdout]
    for line in lines:
        device = basename(line.split()[6])
        if re.match('sdb', device):
            continue
        if re.match('sd*', device):
            full_list_of_devices.append(device)
    return full_list_of_devices



def get_device_to_partition():
    devices_to_make_partition = []
    total_already_presented = []
    already_raw_devices = get_raw_already_created()
    already_lvm_devices = get_lvm_disks()

    total_already_presented = already_raw_devices + already_lvm_devices ## get the devices that were already inthe server BEFORE the add of the new disk, are taken from pvs and raw devices.
    all_present_devices = get_all_present_devices() #I get all devices that are presented in the server after the rescan
    devices_to_make_partition = [i for i in all_present_devices if i not in total_already_presented ]
   # print("a particionar", devices_to_make_partition)
    return devices_to_make_partition


##this below function will get the scsi id for these new devices that were partitioned
def get_scsiid_devices():
    scsi_values = []
    devices_to_partitioning = get_device_to_partition() ##it will get the disk to be partitioned, in order to get their scsi values then.
    lsscsi =  subprocess.Popen(["lsscsi", "-i"],
                               stdout=subprocess.PIPE,
                              )
    lines = [line for line in lsscsi.stdout]
    for line in lines:
        device = basename(line.split()[6])#.decode("utf-8"))
        scsi_id = basename(line.split()[7])#.decode("utf-8") ##it takes the scsi id of each device in the loop
        for x in devices_to_partitioning:
            if x == device:
               # print("este es el device", x)
               # print("este es el scsi", scsi_id)                   
                entry =(x,scsi_id)
                scsi_values.append(entry)   
    
   # print(type(scsi_values[0]))
   # print (scsi_values)
    return scsi_values ##this is a list of tuples with devices+scsi_id for each new devices attached, ths will be used after to edit rules             



raw_already_created = get_raw_already_created()
#print("raw_already_created", raw_already_created)
lvmdevice_already_created = get_lvm_disks() 
#print("lvmdevice_already_created",lvmdevice_already_created)
full_list_of_devices = get_all_present_devices()
#print("full_list_of_devices", full_list_of_devices)
devices_to_make_partition = get_device_to_partition()
#print("devices_to_make_partition", devices_to_make_partition)
devices_to_partitioning = get_scsiid_devices()
print  (devices_to_partitioning)
