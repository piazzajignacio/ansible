#!/usr/bin/env python3.6

from os import listdir
import re
import argparse
from os.path import join, basename

def get_raw_devicename_list():
    my_path = '/dev/oracleasm/'
    lista_raw = listdir(my_path)
    return lista_raw

raws = get_raw_devicename_list()

##it filter these RAW device family that match with the value given, and give you the last devices of mentioned family

def coincidence(family_raw):##here I start to iterate the current list and check if the family RAW device provide by DBA is there
    raw_family_list = []  #here there willbe allocated the RAW devices that match with the one that is provided
    full_raw_list = get_raw_devicename_list()
    global last_device                     ## current RAW devices created in the server
##here below if it is to validate this cases where there is not Coincidence , it means first RAW devices of family
    if not full_raw_list:
        firstraw = ''.join((family_raw,'00'))
        print("este deberia ser el primer elemento", firstraw)
        raw_family_list.append(firstraw)
        last_device = raw_family_list[0]
        return last_device

    if family_raw not in full_raw_list:
        firstraw = ''.join((family_raw,'00'))
        print("este deberia ser el primer elemento", firstraw)
        raw_family_list.append(firstraw)
        last_device = raw_family_list.pop()
        return last_device

    for x in full_raw_list:
        if re.search(family_raw, x):
            print('si esta', x) 
            raw_family_list.append(x)
            raw_family_list.sort()            
            last_device = raw_family_list.pop()
            return last_device


##this function will recieve the output str of coincidence()plus the number of RAW devices to create
#, and will return the names for the RAW devices to be created using the familly patterm 

def raw_device_name_to_create(value, ammount_raw):
    raw_devices_name_to_create = []
    count = 1
  
    while count <= ammount_raw:
        new_number = str(int(value[-2:]) + 1).zfill(2)
        data = value.replace(str(value[-2:]),new_number)
        value = value.replace(str(value),data)
        raw_devices_name_to_create.append(value)
        count = count + 1
    print('list de RAW devices que tienen que ser creados', raw_devices_name_to_create)


def main():
    parser = argparse.ArgumentParser(description='this script will recieve the Family RAW device and the amount of RAW devices you want to create, and will retrieve a list of RAW devices names to be created')
    parser.add_argument('--family_raw', type=str, help='Please provide family of RAW device you want to create, without number, example pure_asm_SDEDTX')
    parser.add_argument('--amount_disks', type=int, help='Put how many disks you want create')
    args = parser.parse_args()
    family_name = args.family_raw
    amount_raw = args.amount_disks
       
    funcion_coincidencia = coincidence(family_name)
    funcion_raw_device_name_to_create = raw_device_name_to_create(funcion_coincidencia,amount_raw)
 
if __name__ == "__main__":
    main()

#device = 'pure_asm_SDEDTX'
#funcion_raw_device_name_to_create = raw_device_name_to_create(funcion_coincidencia,5)
    
    


