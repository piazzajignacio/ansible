#!/usr/bin(env python3.6

import csv 
import operator
import sys 
import os

##esta funcion la hice para que me remueva tuplas vacias de la lista
def remove(tuples):
    tuples = filter(None, tuples)
    return tuples

#Abro el archivo txt y uso el modulo csv para leer cada linea
with open('nombres.txt', 'r') as load_file:
    reader = csv.reader(load_file, delimiter=",")
    data = [tuple(row) for row in reader] #Aca estoy crendo la lista de Tuplas
    data.pop(0) #remuevo la linea de los titulos
    definitive_list = list(remove(data)) #aplico el filter para sacar tuplas vacias
    for index, (x, y) in enumerate(definitive_list): ##Aqui comienzo a correr la lista de uplas y convierto el segundo valor en int por que me venia como str, asi luego se puede ordenar por edad
        definitive_list[index] = (x, int(y))
    
    sort_file = sorted(definitive_list, key=operator.itemgetter(1)) ##aca queda la lista de tuplas ordenada por edad
print(sort_file)

#empiezo a barrer la lista de tupla y comienzo a grabar los valores enn el nuevo file
with open('new_file_ordered', 'w') as new_file:
    for i in sort_file:
        print("esto es i", i)
        line = ' '.join(str(s) for s in i)
        new_file.write(line + '\n')


