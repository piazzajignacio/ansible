#!/usr/bin/env python3.6


import os
import subprocess
from pathlib import Path


def get_raw_already_created():
    raw_already_created = []
    ## we define the ls command
    ls = subprocess.Popen(["ls", "-ltr","/dev/oracleasm/"],
		          stdout=subprocess.PIPE,
                         ) 
    ## we need to definethe grep part
    grep = subprocess.Popen(["awk","-F/", "{print $2}"],
                            stdin=ls.stdout,
                            stdout=subprocess.PIPE,
                           )    
    grep2 = subprocess.Popen(["awk","-F1", "{print $1}"],
                            stdin=grep.stdout,
                            stdout=subprocess.PIPE,
                           )


    endofpipe= grep2.stdout
    
    for line in endofpipe:
        lineedited = line.decode("utf-8").strip("\n")
        #lineedited = line[3:6].decode("utf-8")
        raw_already_created.append(lineedited)
    raw_already_created.pop(0)
    return raw_already_created

def get_lvm_disks():
    lvmdevice_already_created = []
    pvs = subprocess.Popen(["pvs"],
                           stdout=subprocess.PIPE,
                          )
    grep = subprocess.Popen(["awk", "{print $1}"],
                            stdin=pvs.stdout,
                            stdout=subprocess.PIPE,
                           )

    lvm_devices=grep.stdout

    for line in lvm_devices:
        lineedited = line[5:8].decode("utf-8")
        lvmdevice_already_created.append(lineedited)
    lvmdevice_already_created.pop(0) ##remove the firts elment since it is the column PV
    
    return lvmdevice_already_created


def get_all_present_devices():
    full_list_of_devices = [] ## this is the whole device's list presented in the server
    lsscsi =  subprocess.Popen(["lsscsi", "-i"],
                               stdout=subprocess.PIPE,
                              )
    awk = subprocess.Popen(["awk", "-F/dev/", "{print $2}"],
                            stdin=lsscsi.stdout,
                            stdout=subprocess.PIPE,
                          )
    awk2 = subprocess.Popen(["awk", "{print $1}"],
                             stdin=awk.stdout,
                             stdout=subprocess.PIPE,
                           )

    grep = subprocess.Popen(["grep", "-i", "sd"],
                            stdin=awk2.stdout,
                            stdout=subprocess.PIPE,
                           )
    endofpipe = grep.stdout
    for line in endofpipe:
        lineedited = line.decode("utf-8").strip("\n")
        if lineedited != 'sdb':
            full_list_of_devices.append(lineedited)
 
    return full_list_of_devices
    print("full_list_of_devices", full_list_of_devices)

def get_device_to_partition():
    devices_to_make_partition = []
    total_already_presented = []
    already_raw_devices = get_raw_already_created()
    already_lvm_devices = get_lvm_disks()

    total_already_presented = already_raw_devices + already_lvm_devices ## get the devices that were already inthe server BEFORE the add of the new disk, are taken from pvs and raw devices.
    all_present_devices = get_all_present_devices() #I get all devices that are presented in the server after the rescan
    devices_to_make_partition = [i for i in all_present_devices if i not in total_already_presented ]
    print("devices_to_make_partition", devices_to_make_partition)
   # print("a particionar", devices_to_make_partition)   

get_raw_already_created() 
get_lvm_disks()
get_all_present_devices()
get_device_to_partition()
