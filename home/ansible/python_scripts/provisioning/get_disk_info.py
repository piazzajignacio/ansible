#!/usr/bin/env python

import json
import os
import argparse  


datajson = '/home/ansible/json_files/report_disk_localhost.json'

# generar una lista de tupla con todos los scsi ocupados.
VALID_SCSI =[
    (0,1),
    (0,2),
    (0,3),
    (0,4),
    (0,5),
    (0,6),
    (0,8),
    (0,9),
    (0,10),
    (0,11),
    (0,12),
    (0,13),
    (0,14),
    (0,15),
    (1,0),
    (1,1),
    (1,2),
    (1,3),
    (1,4),
    (1,5),
    (1,6),
    (1,8),
    (1,9),
    (1,10),
    (1,11),
    (1,12),
    (1,13),
    (1,14),
    (1,15),
    (2,0),
    (2,1),
    (2,2),
    (2,3),
    (2,4),
    (2,5),
    (2,6),
    (2,8),
    (2,9),
    (2,10),
    (2,11),
    (2,12),
    (2,13),
    (2,14),
    (2,15),
]


##devuelve los scsi busy
def get_busy_scsi():
    busy_scsi = []
    with open(datajson, 'r') as file:
        disc_info = json.load(file)
        result = disc_info['results'][0]
        get_disk_info = result.get('guest_disk_info') ##accediendo a un elemento del diccionario
        for key,value in get_disk_info.items(): ##metodo que devuelve de un diccionario key/value en tuplas
            entry = (value.get('controller_bus_number'), value.get('unit_number')) #creamos una tupla con los dos elementos
            busy_scsi.append(entry)
    return busy_scsi    

def get_available_scsi(valid_scsi):
    available_scsi = []
    busy_scsi = get_busy_scsi()
    for scsi in valid_scsi:
        if scsi not in busy_scsi:
            available_scsi.append(scsi)
    return available_scsi
    
def get_scsi(cantidad):
    available_scsi = get_available_scsi(VALID_SCSI)
    return available_scsi[0:cantidad]

def main():
    parser = argparse.ArgumentParser(description='this will retrieve which scsi are available to attache the disks')
    parser.add_argument('--amount_disks', type=int, help='Please add how many disks you want to create')
    args = parser.parse_args()
    disks_numbers = args.amount_disks
    scsi = get_scsi(disks_numbers)
    print(scsi)

if __name__ == "__main__":
    main()
    
"""
def get_busy_scsi(file_name):
    with open(file_name, 'r') as file:
        disc_info = json.loads(file)
        print(disc_info)
        
    
get_busy_scsi(file_name)
"""
