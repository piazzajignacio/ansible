import csv
import socket
import json

def generate_csvreport(users_not_compliance):
    if len(users_not_compliance) == 0: 
        return     
    path = "{}_user_password_report.csv".format(socket.gethostname())
    field_names = users_not_compliance[0].keys()
    with open(path, 'w') as csv_file:
        thewriter = csv.DictWriter(csv_file, fieldnames=field_names)
        thewriter.writeheader()

        for row_dic in users_not_compliance:
            print(json.dumps(row_dic))
            thewriter.writerow(row_dic)


