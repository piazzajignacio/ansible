#!/usr/bin/env python3.6
"""

Hacer un script  que reciba como argumento el nombre del grupo y que imprima el GID.

hacer test cases.
ver Geneadores

implementacion:
    Hacer una funcion que reciba un array con las lineas del /etc/groups y el nombre del grupo y devuelva el GID


    2DA PARTE:
    UNA FUNCION que reciba el contenido de /etc/passwd y el numero de grupo y devuelva una lista de diccionarios o Tuplas
    con todos los user name, nombre real del usuario  que pertenecen a ese grupo

    3ra parte:

    Una funcion que reciba una lista de usuarios (que viene de la segunda funcion) y devuelva informacion relacionad a password policies

    """
import argparse
import subprocess
import os
import time

from users import get_users_in_group
#from reporte import generate_report
from csv_generator import generate_csvreport

def get_users_not_compliance(user_list, max_pass_age):

    now_in_days = int(time.time() / 86400)
    user_not_compliance = []
    
    with open("/etc/shadow", 'r') as f:
        for user in user_list:
            name = user['Nombre']
            print("procesando user", name)
            f.seek(0)
            for line in f:
                user_shadow = line.rstrip('\n').split(':')[0].ljust(4)
                # print("este es user_shadow", user_shadow)
                if user_shadow == name:
                    chadate = line.rstrip('\n').split(':')[2]
                    if chadate == "":
                        chadate = 0
                    diff_dates = now_in_days - int(chadate)
                    print("dia desde el ultimo cambio", diff_dates)
                    if diff_dates > max_pass_age:
                        user['Diff_days'] = diff_dates
                        user_not_compliance.append(user)
    return user_not_compliance


def main():
    parser = argparse.ArgumentParser(description='this will take the Group provided and will return its GID')
    parser.add_argument('--group_name', type=str, help='This is the name of the group')
    parser.add_argument('--max_password_age', type=int, help='This is the max days without to change password')
    args = parser.parse_args()
    group_name = args.group_name
    max_password_age = args.max_password_age
    users_in_group = get_users_in_group(group_name)
    print("esta es user_list", users_in_group)
    users_not_compliance = get_users_not_compliance(users_in_group, max_password_age)
    print("the users not compliances are:", users_not_compliance)
    #generate_report(users_not_compliance) ##that was for excel file
    generate_csvreport(users_not_compliance)

if __name__ == "__main__":
    main()
