#!/usr/bin/env python3.6

import sys
import socket


def get_os():
    platforms = {
        'linux' : 'Linux',
        'aix'   : 'AIX'
    }
    if sys.platform not in platforms:
        return sys.platform

    return platforms[sys.platform]



def get_hostname():
    return socket.gethostname()

osversion = get_os()
server_name = get_hostname()
print("The server name is : {}, and its OS is : {}".format(server_name,osversion))