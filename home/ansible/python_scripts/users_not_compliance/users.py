#!/usr/bin/env python3.6
import os
import subprocess


def get_group_id(group_name):

    try:
        groups = open('/etc/group', 'r')
        for line in groups:
            fields = line.split(':')
            if fields[0] == group_name:
                print(''.join(fields[2]))
                group_id = fields[2]
                return group_id
        raise SystemExit("The group does not exist")
    except IOError as err:
        print("IOError:", err)
    except Exception as err:
        print("Error:", err)

def get_users_in_group(group_name):
    group_id = get_group_id(group_name)
    users = open('/etc/passwd', 'r')
    user_list = []
    for line in users:
        dict1 = {}
        fields = line.split(':')
        if fields[3] == group_id:
            dict1['Nombre'] = fields[0]
            dict1['Email'] = fields[4]
            user_list.append(dict1)
    return user_list
