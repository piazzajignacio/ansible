import xlsxwriter
import json
import socket

def generate_report(users_not_compliance):

    workbook = xlsxwriter.Workbook("{}_user_password_report.xls".format(socket.gethostname()))
    bold_format = workbook.add_format({'bold': True})


    cell_format = workbook.add_format()
    cell_format.set_text_wrap()
    cell_format.set_align('top')
    cell_format.set_align('left')

    ## format for number field
    """
     dayFormat = workbook.add_format({'num_format':'General'})
    dayRedFormat = workbook.add_format({'num_format':'General'})
    dayRedFormat.set_font_color('red')
 
    """



    ##  we describe the header of the table
    worksheet = workbook.add_worksheet('users list')

    worksheet.write('A1', 'Name of user', bold_format)
    worksheet.write('B1', 'Email', bold_format)
    worksheet.write('C1', 'Days without change password', bold_format)

    rawIndex = 2


    for dic in users_not_compliance:
        print(json.dumps(dic))
        name = dic['Nombre']
        email = dic['Email']
        days_since_last_change = dic['Diff_days']

        worksheet.write('A' + str(rawIndex), name, cell_format)
        worksheet.write('B' + str(rawIndex), email, cell_format)
        worksheet.write('C' + str(rawIndex), days_since_last_change, cell_format)
        worksheet.write('C' + str(rawIndex), days_since_last_change, cell_format)


        rawIndex += 1


    workbook.close()




